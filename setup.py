#!/usr/bin/env python
from setuptools import setup

setup(
    name='Dark Energy and Dark Matter Simulations',
    version='0.0.1',
    packages=['darksim'],
    description='Binance REST API python implementation',
    url='https://github.com/sammchardy/python-binance',
    author='Carson West, Carol-Anne Collins, Michael Santrock, Rohan Srivastava, Ruike Li',
    license='MIT',
    author_email='',
    install_requires=['numpy', 'pandas', 'dask[complete]'],
    keywords='binance exchange rest api bitcoin ethereum btc eth neo'
)
