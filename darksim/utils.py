import numpy as np



class integration:
    """ Integration methods """
    def _gaussxw(N):
        # Initial approximation to roots of the Legendre polynomial
        a = np.linspace(3,4*N-1,N)/(4*N+2)
        x = np.cos(np.pi*a+1/(8*N*N*np.tan(a)))

        # Find roots using Newton's method
        epsilon = 1e-15
        delta = 1.0
        while delta>epsilon:
            p0 = np.ones(N,float)
            p1 = np.copy(x)
            for k in range(1,N):
                p0,p1 = p1,((2*k+1)*x*p1-k*p0)/(k+1)
            dp = (N+1)*(p0-x*p1)/(1-x*x)
            dx = p1/dp
            x -= dx
            delta = max(abs(dx))

        # Calculate the weights
        w = 2*(N+1)*(N+1)/(N*N*(1-x*x)*dp*dp)
        return x,w


        return 0.5*(b-a)*x+0.5*(b+a), 0.5*(b-a)*w


    def gauss(fxn, a, b, steps):
        # Get points and weights
        points, weights = integration._gaussxw(steps)

        # Scale points and weights
        points, weights = 0.5*(b-a)*points+0.5*(b+a), 0.5*(b-a)*weights

        return np.sum(fxn(points) * weights)



def gen_test_data(n=100, clusters=2, scale=1e21, v_scale=1e7, m_scale=1e35):
    """ Generate test data for simulation

    Generates (x, y, z) position components, (v_x, v_y, v_z) velocity components, and masses.
    Positions follow the structure seen in superclusters, where regions of densly packed objects
    in a roughly spherical distribution, with decreasing density as R increases, where each of
    these `clusters` are seperated by a relativity large distance. Velocities follow a similar
    pattern where closer to R = 0 velocities are lower and increase logarithmically. Masses
    are calculated proportionally to their velocity magnitude.

    Scale values are meant to represent accurate scales at the simulation scale in SI units. For
    exampe, scale is meant to be on the scale of 100,000 light years, velocity on the scale of
    10,000 km/s, and mass on 100,000 solar mass scales for default values.

    n (int, 100): Number of objects per cluster to calculate.
    clusters (int, 2): Number of clusters to calculate. Total number of objects will be
        n * clusters
    scale (float, 1e21): Position scale
    v_scale (float, 1e7): Velocity scale
    m_scale (float, 1e35): Mass scale

    """
    gen = lambda offset: np.random.rayleigh(scale, n) * np.random.normal(0, 100, size=n) * (np.random.random(size=n)) + offset

    # Positions
    tempx = []
    tempy = []
    tempz = []
    tempvx = []
    tempvy = []
    tempvz = []
    for i in range(clusters):
        if i == 0:
            offset = 0
        elif i % 2 == 1:
            offset = scale * 500 / i
        else:
            offset = -scale * 750 / i
        print(offset)
        x = tempx.append(gen(offset))
        y = tempy.append(gen(offset))
        z = tempz.append(gen(offset))

    x = np.concatenate(tuple(tempx))
    y = np.concatenate(tuple(tempy))
    z = np.concatenate(tuple(tempz))

    # Velocities
    vx = x.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)
    vy = y.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)
    vz = z.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)

    m = np.power(np.power(vx, 2) + np.power(vx, 2) + np.power(vx, 2), 1/2) * m_scale

    return [[x,y,z], [vx,vy,vz], m]
