import numpy as np
import matplotlib.pyplot as plt

def gen_test_data(n=100, clusters=2, scale=1e21, v_scale=1e6, m_scale=1e15):
    gen = lambda offset: np.random.rayleigh(scale, n) * np.random.normal(0, 100, size=n) * (np.random.random(size=n)) + offset

    # Positions
    tempx = []
    tempy = []
    tempz = []
    tempvx = []
    tempvy = []
    tempvz = []
    for i in range(clusters):
        if i == 0:
            offset = 0
        elif i % 2 == 1:
            offset = scale * 500 / i
        else:
            offset = -scale * 750 / i
        print(offset)
        x = tempx.append(gen(offset))
        y = tempy.append(gen(offset))
        z = tempz.append(gen(offset))

    x = np.concatenate(tuple(tempx))
    y = np.concatenate(tuple(tempy))
    z = np.concatenate(tuple(tempz))

    # Velocities
    vx = x.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)
    vy = y.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)
    vz = z.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)

    m = np.power(np.power(vx, 2) + np.power(vx, 2) + np.power(vx, 2), 1/2) * m_scale

    return [[x,y,z], [vx,vy,vz], m]



if __name__ == '__main__':
    result = gen_test_data()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_zlabel('z')
    ax.set_ylabel('y')
    ax.set_xlabel('x')

    ax.scatter(*result[0])
    plt.show()
